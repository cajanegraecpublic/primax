-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2017 at 01:40 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `primax`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `persona` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `persona`, `correo`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Iván Mera', 'imera92@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `correo_config`
--

CREATE TABLE IF NOT EXISTS `correo_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servidor_correo` varchar(50) NOT NULL,
  `puerto` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geo_ciudad`
--

CREATE TABLE IF NOT EXISTS `geo_ciudad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=221 ;

--
-- Dumping data for table `geo_ciudad`
--

INSERT INTO `geo_ciudad` (`id`, `nombre`, `id_provincia`) VALUES
(1, 'CUENCA', 1),
(2, 'GIRON', 1),
(3, 'GUALACEO', 1),
(4, 'NABON', 1),
(5, 'PAUTE', 1),
(6, 'PUCARA', 1),
(7, 'SAN FERNANDO', 1),
(8, 'SANTA ISABEL', 1),
(9, 'SIGSIG', 1),
(10, 'OÑA', 1),
(11, 'CHORDELEG', 1),
(12, 'EL PAN', 1),
(13, 'SEVILLA DE ORO', 1),
(14, 'GUACHAPALA', 1),
(15, 'CAMILO PONCE ENRIQUEZ', 1),
(16, 'GUARANDA', 2),
(17, 'CHILLANES', 2),
(18, 'ECHEANDIA', 2),
(19, 'SAN MIGUEL', 2),
(20, 'CALUMA', 2),
(21, 'LAS NAVES', 2),
(22, 'AZOGUES', 3),
(23, 'BIBLIAN', 3),
(24, 'CAÑAR', 3),
(25, 'LA TRONCAL', 3),
(26, 'EL TAMBO', 3),
(27, 'DELEG', 3),
(28, 'SUSCAL', 3),
(29, 'TULCAN', 4),
(30, 'BOLIVAR', 4),
(31, 'ESPEJO', 4),
(32, 'MIRA', 4),
(33, 'MONTUFAR', 4),
(34, 'SAN PEDRO DE HUACA', 4),
(35, 'RIOBAMBA', 5),
(36, 'ALAUSI', 5),
(37, 'COLTA', 5),
(38, 'CHAMBO', 5),
(39, 'CHUNCHI', 5),
(40, 'GUAMOTE', 5),
(41, 'GUANO', 5),
(42, 'PALLATANGA', 5),
(43, 'PENIPE', 5),
(44, 'CUMANDA', 5),
(45, 'LATACUNGA', 6),
(46, 'LA MANA', 6),
(47, 'PANGUA', 6),
(48, 'PUJILI', 6),
(49, 'SALCEDO', 6),
(50, 'SAQUISILI', 6),
(51, 'SIGCHOS', 6),
(52, 'MACHALA', 7),
(53, 'ARENILLAS', 7),
(54, 'ATAHUALPA', 7),
(55, 'BALSAS', 7),
(56, 'CHILLA', 7),
(57, 'EL GUABO', 7),
(58, 'HUAQUILLAS', 7),
(59, 'MARCABELI', 7),
(60, 'PASAJE', 7),
(61, 'PIÑAS', 7),
(62, 'PORTOVELO', 7),
(63, 'SANTA ROSA', 7),
(64, 'ZARUMA', 7),
(65, 'LAS LAJAS', 7),
(66, 'ESMERALDAS', 8),
(67, 'ELOY ALFARO', 8),
(68, 'MUISNE', 8),
(69, 'QUININDE', 8),
(70, 'SAN LORENZO', 8),
(71, 'ATACAMES', 8),
(72, 'RIOVERDE', 8),
(73, 'LA CONCORDIA', 8),
(74, 'SAN CRISTÓBAL', 9),
(75, 'ISABELLA', 9),
(76, 'SANTA CRUZ', 9),
(77, 'GUAYAQUIL', 10),
(78, 'ALFREDO BAQUERIZO MORENO', 10),
(79, 'BALAO', 10),
(80, 'BALZAR', 10),
(81, 'COLIMES', 10),
(82, 'DAULE', 10),
(83, 'DURAN', 10),
(84, 'EL EMPALME', 10),
(85, 'EL TRIUNFO', 10),
(86, 'MILAGRO', 10),
(87, 'NARANJAL', 10),
(88, 'NARANJITO', 10),
(89, 'PALESTINA', 10),
(90, 'PEDRO CARBO', 10),
(91, 'SAMBORONDON', 10),
(92, 'SANTA LUCIA', 10),
(93, 'URBINA JADO', 10),
(94, 'YAGUACHI', 10),
(95, 'PLAYAS', 10),
(96, 'SIMON BOLIVAR', 10),
(97, 'CORONEL MARCELINO MARIDUEÑA', 10),
(98, 'LOMAS DE SARGENTILLO', 10),
(99, 'NOBOL', 10),
(100, 'GENERAL ANTONIO ELIZALDE', 10),
(101, 'ISIDRO AYORA', 10),
(102, 'IBARRA', 11),
(103, 'ANTONIO ANTE', 11),
(104, 'COTACACHI', 11),
(105, 'OTAVALO', 11),
(106, 'PIMAMPIRO', 11),
(107, 'SAN MIGUEL DE URCUQUI', 11),
(108, 'LOJA', 12),
(109, 'CALVAS', 12),
(110, 'CATAMAYO', 12),
(111, 'CELICA', 12),
(112, 'CHAGUARPAMBA', 12),
(113, 'ESPINDOLA', 12),
(114, 'GONZANAMA', 12),
(115, 'MACARA', 12),
(116, 'PALTAS', 12),
(117, 'PUYANGO', 12),
(118, 'SARAGURO', 12),
(119, 'SOZORANGA', 12),
(120, 'ZAPOTILLO', 12),
(121, 'PINDAL', 12),
(122, 'QUILANGA', 12),
(123, 'OLMEDO', 12),
(124, 'BABA', 13),
(125, 'BABAHOYO', 13),
(126, 'BUENA FE', 13),
(127, 'MOCACHE', 13),
(128, 'MONTALVO', 13),
(129, 'PALENQUE', 13),
(130, 'PUEBLOVIEJO', 13),
(131, 'QUEVEDO', 13),
(132, 'QUINSALOMA', 13),
(133, 'URDANETA', 13),
(134, 'VALENCIA', 13),
(135, 'VENTANAS', 13),
(136, 'VINCES', 13),
(137, '24 DE MAYO', 14),
(138, 'BOLIVAR', 14),
(139, 'CHONE', 14),
(140, 'EL CARMEN', 14),
(141, 'FLAVIO ALFARO', 14),
(142, 'JAMA', 14),
(143, 'JARAMIJO', 14),
(144, 'JIPIJAPA', 14),
(145, 'JUNIN', 14),
(146, 'MANTA', 14),
(147, 'MONTECRISTI', 14),
(148, 'OLMEDO', 14),
(149, 'PAJAN', 14),
(150, 'PEDERNALES', 14),
(151, 'PICHINCHA', 14),
(152, 'PORTOVIEJO', 14),
(153, 'PUERTO LOPEZ', 14),
(154, 'ROCAFUERTE', 14),
(155, 'SAN VICENTE', 14),
(156, 'SANTA ANA', 14),
(157, 'SUCRE', 14),
(158, 'TOSAGUA', 14),
(159, 'GUALAQUIZA', 15),
(160, 'HUAMBOYA', 15),
(161, 'LIMON INDIANZA', 15),
(162, 'LOGROÑO', 15),
(163, 'MORONA', 15),
(164, 'PABLO VI', 15),
(165, 'PALORA', 15),
(166, 'SAN JUAN BOSCO', 15),
(167, 'SANTIAGO', 15),
(168, 'SUCUA', 15),
(169, 'TAISHA', 15),
(170, 'TIWINTZA', 15),
(171, 'ARCHIDONA', 16),
(172, 'CARLOS JULIO AROSEMENA', 16),
(173, 'EL CHACO', 16),
(174, 'QUIJOS', 16),
(175, 'TENA', 16),
(176, 'AGUARICO', 17),
(177, 'LA JOYA DE LOS SACHAS', 17),
(178, 'LORETO', 17),
(179, 'ORELLANA', 17),
(180, 'ARANJUNO', 18),
(181, 'MERA', 18),
(182, 'PASTAZA', 18),
(183, 'SANTA CLARA', 18),
(184, 'CAYAMBE', 19),
(185, 'MEJIA', 19),
(186, 'PEDRO MONCAYO', 19),
(187, 'PEDRO VICENTE MALDONADO', 19),
(188, 'PUERTO QUITO', 19),
(189, 'QUITO', 19),
(190, 'RUMIÑAHUI', 19),
(191, 'SAN MIGUEL DE LOS BANCOS', 19),
(192, 'LIBERTAD', 23),
(193, 'SALINAS', 23),
(194, 'SANTA ELENA', 23),
(195, 'SANTO DOMINGO DE LOS COLORADOS', 24),
(196, 'CASCALES', 20),
(197, 'CUYABENO', 20),
(198, 'GONZALO PIZARRO', 20),
(199, 'LAGO AGRIO', 20),
(200, 'PUTUMAYO', 20),
(201, 'SHUSHUFINDI', 20),
(202, 'SUCUMBIOS', 20),
(203, 'AMBATO', 21),
(204, 'BAÑOS', 21),
(205, 'CEVALLOS', 21),
(206, 'MOCHA', 21),
(207, 'PATATE', 21),
(208, 'QUERO', 21),
(209, 'SAN PEDRO DE PELILEO', 21),
(210, 'SANTIAGO DE PILLARO', 21),
(211, 'TISALEO', 21),
(212, 'CENTINELA DEL CONDOR', 22),
(213, 'CHINCHIPE', 22),
(214, 'EL PANGUI', 22),
(215, 'NANGARITZA', 22),
(216, 'PALANDA', 22),
(217, 'PAQUISHA', 22),
(218, 'YACUAMBI', 22),
(219, 'YANTZAZA', 22),
(220, 'ZAMORA', 22);

-- --------------------------------------------------------

--
-- Table structure for table `geo_pais`
--

CREATE TABLE IF NOT EXISTS `geo_pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sortname` (`sortname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `geo_pais`
--

INSERT INTO `geo_pais` (`id`, `sortname`, `name`) VALUES
(1, 'EC', 'Ecuador');

-- --------------------------------------------------------

--
-- Table structure for table `geo_provincia`
--

CREATE TABLE IF NOT EXISTS `geo_provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `id_pais` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `geo_provincia`
--

INSERT INTO `geo_provincia` (`id`, `nombre`, `id_pais`) VALUES
(1, 'Azuay', 1),
(2, 'Bolivar', 1),
(3, 'Cañar', 1),
(4, 'Carchi', 1),
(5, 'Chimborazo', 1),
(6, 'Cotopaxi', 1),
(7, 'El Oro', 1),
(8, 'Esmeraldas', 1),
(9, 'Galapagos', 1),
(10, 'Guayas', 1),
(11, 'Imbabura', 1),
(12, 'Loja', 1),
(13, 'Los Rios', 1),
(14, 'Manabi', 1),
(15, 'Morona Santiago', 1),
(16, 'Napo', 1),
(17, 'Orellana', 1),
(18, 'Pastaza', 1),
(19, 'Pichincha', 1),
(20, 'Sucumbios', 1),
(21, 'Tungurahua', 1),
(22, 'Zamora Chinchipe', 1),
(23, 'Santa Elena', 1),
(24, 'Santo Domingo de los Tsáchilas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participaciones`
--

CREATE TABLE IF NOT EXISTS `participaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `cedula` varchar(10) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `ingresado` int(11) NOT NULL,
  `factura` varchar(100) NOT NULL,
  `fb_share` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
