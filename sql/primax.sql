-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `persona` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `admin` (`id`, `user`, `password`, `persona`, `correo`) VALUES
(1,	'admin',	'21232f297a57a5a743894a0e4a801fc3',	'Iván Mera',	'imera92@gmail.com');

DROP TABLE IF EXISTS `correo_config`;
CREATE TABLE `correo_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servidor_correo` varchar(50) NOT NULL,
  `puerto` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `geo_ciudad`;
CREATE TABLE `geo_ciudad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `geo_ciudad` (`id`, `nombre`, `id_provincia`) VALUES
(1,	'Amaguaña',	1),
(2,	'Ambato',	1),
(3,	'Archidona',	1),
(4,	'Atuntaqui',	1),
(5,	'Azogues',	1),
(6,	'Babahoyo',	1),
(7,	'Bahía de Caraquez',	1),
(8,	'Baños',	1),
(9,	'Baquerizo Moreno',	1),
(10,	'Cascol',	1),
(11,	'Catamayo',	1),
(12,	'Cayambe',	1),
(13,	'Chimbo',	1),
(14,	'Cuenca',	1),
(15,	'Daule',	1),
(16,	'Duran',	1),
(17,	'El Carmen',	1),
(18,	'El Chaco',	1),
(19,	'El Empalme',	1),
(20,	'El Guabo',	1),
(21,	'Esmeraldas',	1),
(22,	'Girón',	1),
(23,	'Gualaceo',	1),
(24,	'Guayaquil',	1),
(25,	'Huaquillas',	1),
(26,	'Ibarra',	1),
(27,	'Isidro Ayora',	1),
(28,	'Jipijapa',	1),
(29,	'La Libertad',	1),
(30,	'La Troncal',	1),
(31,	'Loja',	1),
(32,	'Machachi',	1),
(33,	'Machala',	1),
(34,	'Manta',	1),
(35,	'Mejia',	1),
(36,	'Milagro',	1),
(37,	'Montecristi',	1),
(38,	'Naranjal',	1),
(39,	'Pajan',	1),
(40,	'Patricia Pilar',	1),
(41,	'Paute',	1),
(42,	'Pedernales',	1),
(43,	'Pedro Carbo',	1),
(44,	'Pifo',	1),
(45,	'Playas',	1),
(46,	'Portoviejo',	1),
(47,	'Progreso',	1),
(48,	'Quininde',	1),
(49,	'Quito',	1),
(50,	'Quito (Sector Conocoto)',	1),
(51,	'Rio Negro',	1),
(52,	'Riobamba',	1),
(53,	'Rocafuerte',	1),
(54,	'Rumiñahui',	1),
(55,	'Salcedo',	1),
(56,	'Samborondon',	1),
(57,	'San Rafael',	1),
(58,	'San Vicente',	1),
(59,	'Santa Elena',	1),
(60,	'Santa Isabel',	1),
(61,	'Santo Domingo',	1),
(62,	'Shushufindi',	1),
(63,	'Sucre',	1),
(64,	'Tabacundo',	1),
(65,	'Tumbaco',	1),
(66,	'Vinces',	1),
(67,	'Yaguachi',	1),
(68,	'Zaruma',	1);

DROP TABLE IF EXISTS `geo_pais`;
CREATE TABLE `geo_pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sortname` (`sortname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `geo_pais` (`id`, `sortname`, `name`) VALUES
(1,	'EC',	'Ecuador');

DROP TABLE IF EXISTS `geo_provincia`;
CREATE TABLE `geo_provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `id_pais` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `geo_provincia` (`id`, `nombre`, `id_pais`) VALUES
(1,	'Azuay',	1),
(2,	'Bolivar',	1),
(3,	'Cañar',	1),
(4,	'Carchi',	1),
(5,	'Chimborazo',	1),
(6,	'Cotopaxi',	1),
(7,	'El Oro',	1),
(8,	'Esmeraldas',	1),
(9,	'Galapagos',	1),
(10,	'Guayas',	1),
(11,	'Imbabura',	1),
(12,	'Loja',	1),
(13,	'Los Rios',	1),
(14,	'Manabi',	1),
(15,	'Morona Santiago',	1),
(16,	'Napo',	1),
(17,	'Orellana',	1),
(18,	'Pastaza',	1),
(19,	'Pichincha',	1),
(20,	'Sucumbios',	1),
(21,	'Tungurahua',	1),
(22,	'Zamora Chinchipe',	1),
(23,	'Santa Elena',	1),
(24,	'Santo Domingo de los Tsáchilas',	1);

DROP TABLE IF EXISTS `participaciones`;
CREATE TABLE `participaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `cedula` varchar(10) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `ingresado` int(11) NOT NULL,
  `factura` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `participaciones` (`id`, `nombre`, `apellido`, `cedula`, `celular`, `correo`, `fecha_nacimiento`, `id_ciudad`, `ingresado`, `factura`, `estado`) VALUES
(46,	'Estefania',	'Monge',	'0926021692',	'0987272385',	'ing.estefania.monge@gmail.com',	'1988-03-01',	24,	1506434464,	'8304316ee153a6bcdb5ccf88bbdb0265.jpg',	2),
(47,	'Cesar',	'Moncayo',	'0930324751',	'0998696907',	'cesar.moncayo@hotmail.com',	'1992-03-24',	24,	1506434577,	'a86086b3fd6f7e77743af3f5a2c9af5c.jpg',	2),
(48,	'esaud edwin',	'sanchez salavarria',	'0919911891',	'0969621474',	'edwinsanz@hotmail.com',	'1983-08-24',	24,	1506435068,	'f1ba62b79a3a7424d515294c9d0fd27b.jpg',	2),
(49,	'Carlos Javier',	'García Morán',	'0920661287',	'0998521052',	'carlosg_2580@hotmail.com',	'1980-10-18',	24,	1506447616,	'eba6962bb6bce9d735ffce9183e487b5.jpg',	1),
(50,	'Carlos Javier',	'García Morán',	'0920661287',	'0998521052',	'carlosg_2580@hotmail.com',	'1980-10-18',	24,	1506447659,	'e69170d42a29997d3d62fae22d827548.jpg',	1),
(51,	'Juan Carlos',	'Suarez Fernandex',	'0959622226',	'0996141456',	'juancasuarez83@gmail.com',	'1983-04-11',	46,	1506448477,	'f71cd69ba7d44c7a354e1e330936867a.jpg',	1),
(52,	'Juan Carlos',	'Suarez Fernandex',	'0959622226',	'0996141456',	'juancasuarez83@gmail.com',	'1983-04-11',	46,	1506448991,	'285c9ab7f86ca7a4780bc56a5cdcfc00.jpg',	1),
(53,	'Juan Carlos',	'Suarez Fernandez',	'0959622226',	'0996141456',	'juancasuarez83@gmail.com',	'1983-04-11',	46,	1506450680,	'16d4c221d4db71bf75d1d42980d3702d.jpg',	1);

-- 2017-09-26 18:44:11
