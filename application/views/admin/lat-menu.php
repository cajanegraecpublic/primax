        <div id="wrapper">
            <!-- #sidebar-wrapper -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="<?php echo base_url('admin') ?>">
                                        <img class="img-responsive" src="<?php echo base_url('public/img/logo_horizontal_transparente.png'); ?>">
                                    </a>                                    
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/administrar_participaciones'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Participaciones</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</a>
                    </li>
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->
