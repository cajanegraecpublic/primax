            <!-- #page-content-wrapper -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="index-box">
                                <h1 class="mt-10">Bienvenido al Administrador de PRIMAX</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-10 mb-10">
                        <div class="col-md-12 text-center">
                            <table class="table table-hover" id="tablaAprobados">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Correo</th>
                                        <th>Celular</th>
                                        <th>Cédula</th>
                                        <th>Fecha de Nacimiento</th>
                                        <th>Fecha de Registro</th>
                                        <th>Ciudad</th>
                                        <th>Compartir</th>
                                        <th>Imagen de la Factura</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($participantes as $index => $participante): ?>
                                    <tr>
                                        <td> <?php echo ($index + 1); ?> </td>
                                        <td> <?php echo $participante["nombre"]; ?> </td>
                                        <td> <?php echo $participante["apellido"]; ?> </td>
                                        <td> <?php echo $participante["correo"]; ?> </td>
                                        <td> <?php echo $participante["celular"]; ?> </td>
                                        <td> <?php echo $participante["cedula"]; ?> </td>
                                        <td> <?php echo $participante["fecha_nacimiento"]; ?> </td>
                                        <td> <?php echo date("Y-m-d H:i:s", $participante["ingresado"]); ?> </td>
                                        <td> <?php echo $participante["ciudad"]; ?> </td>
                                        <td> <?php echo $participante["fb_share"] == 1 ? "Sí" : "No"; ?> </td>
                                        <td> <a target="_blank" href="<?php echo base_url("assets/facturas") . "/" . $participante["factura"]; ?>"><?php echo base_url("assets/facturas") . "/" . $participante["factura"]; ?></a> </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row mb-10">
                        <div class="col-md-2">
                            <button type="button" id="eliminarDatos">Eliminar</button>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-content-wrapper -->