        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel">
                        <div class="panel-heading text-center">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <img class="img-responsive img-center" src="<?php echo base_url('public/img/logo_horizontal.png'); ?>">
                                    </div>
                                </div>
                                <div class="row mt-20">
                                    <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-md-8 col-md-offset-2">
                                        <img class="img-responsive img-center" src="<?php echo base_url('public/img/logo_promo2.png'); ?>">
                                    </div>
                                </div>
                            </div>
                            <h4 class="panel-title pt-20">Por favor ingrese su usuario y contraseña:</h4>
                        </div>
                        <div class="panel-body">
                            <?php echo form_open('admin/auth' , array('class' => 'form-horizontal', 'id' => 'frm-login')); ?>  
                                <fieldset>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <?php echo form_input(array(
                                            'name' => 'user',
                                            'value' => '',
                                            'placeholder' => 'Usuario',
                                            'class' => 'form-control',
                                        ));?>
                                    </div>
                                    <div class="clearfix"></div><br>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <?php echo form_password(array(
                                            'name' => 'password',
                                            'value' => '',
                                            'placeholder' => 'contraseña',
                                            'class' => 'form-control',
                                        ));?>
                                    </div>
                                    <div class="clearfix"></div><br>
                                    <div class="input-group">
                                        <button type="submit" class="btn btn-login">Iniciar sesión</button>
                                    </div>
                                </fieldset>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>