			<script>
				window.fbAsyncInit = function() {
					FB.init({
						appId      : '131098367531778',
						cookie     : true,  // enable cookies to allow the server to access 
											// the session
						xfbml      : true,  // parse social plugins on this page
						version    : 'v2.10' // use graph api version 2.8
					});

					// Now that we've initialized the JavaScript SDK, we call 
					// FB.getLoginStatus().  This function gets the state of the
					// person visiting this page and can return one of three states to
					// the callback you provide.  They can be:
					//
					// 1. Logged into your app ('connected')
					// 2. Logged into Facebook, but not your app ('not_authorized')
					// 3. Not logged into Facebook and can't tell if they are logged into
					//    your app or not.
					//
					// These three cases are handled in the callback function.
				};

				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=131098367531778";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>

			<!-- WEB -->
			<div class="row hidden-xs hidden-sm">
				<div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 ">
					<img class="img-responsive" src="<?php echo base_url('public/img/bravo-web.png') ?>" alt="Primax">
					<a class="btn btn-social btn-facebook" id="shareBtn" >
						<span class="fa fa-facebook"></span> Compartir
					</a>
				</div>
			</div>
			<!-- Movil -->
			<div class="row hidden-lg hidden-md mb-20">
				<div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 ">
					<img class="img-responsive" src="<?php echo base_url('public/img/bravo-movil.png') ?>" alt="Primax">
					<a class="btn btn-social btn-facebook" id="shareBtnMobile" >
						<span class="fa fa-facebook"></span> Compartir
					</a>
				</div>
			</div>

			<?php /*
			
			<div class="row hidden-xs hidden-sm">
				<div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 ">
					<h1 class="titular-h1 text-center">&#161;BRAVO!</h1>
				</div>
			</div>
			<div class="row hidden-lg hidden-md mb-20">
				<div class="col-xs-6 col-xs-offset-6">
					<div class="row">
						<img class="img-responsive div-logo-horizontal-mobile" src="<?php echo base_url('public/img/logo_horizontal.png') ?>" alt="Primax">
					</div>
				</div>
			</div>
			<div class="row hidden-lg hidden-md">
				<div class="col-xs-5 col-xs-offset-7">
					<div class="row">
						<img class="img-responsive pull-right" src="<?php echo base_url('public/img/segundaMobile.png'); ?>">
					</div> 
				</div>
			</div>
			<div class="row hidden-lg hidden-md">
				<div class="col-xs-12 ">
					<div class="titular-h1-mobile text-center">&#161;BRAVO!</div>
				</div>
			</div>
			<div class="row hidden-lg hidden-md">
				<div class="col-xs-5">
					<div class="row">
						<img class="img-responsive pull-right" src="<?php echo base_url('public/img/segundaMobile.png'); ?>">
					</div> 
				</div>
			</div>
			<div class="row hidden-lg hidden-md">
				<div class="col-xs-8 col-xs-offset-2">
					<div class="titular-h2-mobile">YA ESTÁS</div>
					<div class="titular-h2-mobile">PARTICIPANDO </div>
					<div class="titular-h2-mobile">POR UN AÑO DE</div>
					<div class="titular-h2-mobile">SUPER G-PRIX</div>
				</div>
			</div>
			<div id="lower-container-mobile" class="row hidden-lg hidden-md">
				<div class="col-xs-8 col-xs-offset-2">
					<div class="titular-h3-mobile">&#191;Quieres doblar</div>
					<div class="titular-h3-mobile">las oportunidades</div>
					<div class="titular-h3-mobile">de ganar con Primax?</div>
					<div class="titular-h3-mobile"><div class="btn btn-success" id="shareBtn" style="display: none;">Compartir</div></div>
				</div>
			</div>
			<div id="lower-container" class="row mt-30 hidden-sm hidden-xs">
				<div class="col-xs-7 col-lg-6 col-md-6">
					<div class="row">
						<div class="col-xs-12">
							<img class="pull-right img-responsive img-center" src="<?php echo base_url('public/img/texto-participando.png'); ?>" alt="Consume $10 o más de Super G-Prix">
						</div>
						
					</div>
				</div>
				<div class="col-xs-5 col-lg-6 col-md-6  contenedor-centrar">
					<div class="row">
						<div class="col-lg-8 col-md-8">
							<h3 class="titular-h3">&#191;Quieres doblar <br> las oportunidades<br> de ganar con Primax?</h3>
						</div>
						<div class="col-lg-4 col-md-4 contenedor-centrar">
							<div class="btn btn-success" id="shareBtnMobile" style="display: none;">Compartir</div>
						</div>
					</div>
					<!-- <div class="row mb-10">
						<div class="col-xs-8 col-xs-offset-2 text-center">
							<div class="btn btn-success" id="shareBtnMobile">Compartir</div>
						</div>
					</div> -->
				</div>
			</div>
			<div class="row mt-20">
				<div class="col-xs-8 col-xs-offset-2 col-lg-10 col-lg-offset-1">
					<img class="img-responsive img-center" src="<?php echo base_url('public/img/footerBravo.png'); ?>">
				</div>
			</div>

			*/ ?>