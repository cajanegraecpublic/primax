		<footer class="footer">
			<div id="lower-footer-mobile" class="row pt-10 pb-20 hidden-md hidden-lg hidden-md text-center">
				<div class="col-xs-2 col-xs-offset-3 mt-10 mb-10">
					<a href="https://www.facebook.com/PrimaxEcuador/" target="_blank" class="icon-redes-wrapper"><span class="icon-redes-fb icon-redes"></span></a>
				</div>
				<div class="col-xs-2 mt-10 mb-10">
					<a href="https://www.instagram.com/primaxec/" target="_blank" class="icon-redes-wrapper"><span class="icon-redes-ig icon-redes"></span></a>
				</div>
				<div class="col-xs-2 mt-10 mb-10">
					<a href="https://www.youtube.com/channel/UCim4K4o2nv2bRrtESHdk-dg" target="_blank" class="icon-redes-wrapper"><span class="icon-redes-yt icon-redes"></span></a>
				</div>
				<div class="col-xs-12 text-center terms-and-conditions-mobile">
					<a href="<?php echo base_url('public/docs/terminos.pdf'); ?>" target="_blank">
						Conoce los términos y condiciones del concurso
					</a>
					<br />
					&copy; 2017 Primax Ecuador. Todos los derechos reservados
				</div>
			</div>
		</footer>
		</div> <!-- /.container-fluid -->
	</body>
	<!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script> -->
    <script src="<?php echo base_url('public/js/jquery-3.2.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/js/bootstrap.min.js'); ?>"></script>
    <?php if ($this->router->method == 'form_process'): ?>
    	<script src="<?php echo base_url('public/js/form.js'); ?>"></script>
	<?php elseif ($this->router->method == 'index'): ?>
		<script src="<?php echo base_url('public/js/index.js'); ?>"></script>
	<?php elseif ($this->router->method == 'exito'): ?>
		<script>
			function centrarVert(elemento){
				altura1 = elemento.parent().height();
				console.log(altura1);
				altura2=elemento.height();
				// altura2 = elemento.children('a').height();
				console.log(altura2);
				elemento.css('margin-top',(altura1/2)-(altura2/2)+'px');
				console.log((altura1/2)-(altura2/2)+'px');
			}
			function centrarVertP(elemento){
				altura1 = elemento.parent().height();
				console.log(altura1);
				altura2=elemento.height();
				// altura2 = elemento.children('a').height();
				console.log(altura2);
				elemento.css('padding-top',(altura1/2)-(altura2/2)+'px');
				console.log((altura1/2)-(altura2/2)+'px');
			}
			$(window).on('resize', function () {
				$('#upper-right-col div').height($('#upper-left-col').height());
				$('.contenedor-centrar').each(function () {
					centrarVert($(this));
					console.log('ok');
				});
				$('.contenedor-centrarP').each(function () {
					centrarVertP($(this));
					console.log('ok');
				});
			});
			$(document).ready(function() {
				$('.contenedor-centrar').each(function () {
					centrarVert($(this));
					console.log('ok');
				});
				$('.contenedor-centrarP').each(function () {
					centrarVertP($(this));
					console.log('ok');
				});
				
				$.ajax({
					url: js_site_url('web/sendMail')
				})
				.done(function(data) {
					console.log(data);
				})
				.fail(function() {
					console.log("error");
				});

				window.setTimeout(function () {
					FB.getLoginStatus(function(response) {
						// if (response.status !== 'connected') {
							// $('#shareBtn, #shareBtnMobile').show();
						// } else {
							// $('#shareBtn, #shareBtnMobile').hide();
							$('#shareBtn, #shareBtnMobile').click(function(event) {
								FB.ui({
									method: 'share',
									display: 'popup',
									href: 'http://www.supergprix.com/',
								}, function(response){
									if (response) {
										$.ajax({
											url: js_site_url('form/complete_fill_in')
										})
										.done(function(response) {
											console.log(response);
											if (response.codigo == 1) {
												// complete
												console.log("complete");
											} else {
												// something bad happened :(
												console.log("something bad happened :(");
											}
										})
										.fail(function() {
											// something bad happened :(
											console.log("something bad happened :(");
										});
									} else {
										// a kitty just died :(
										console.log("a kitty just died :(");
									}
								});
							});
						// }
					});
				}, 5000);
			});
		</script>
	<?php endif; ?>




	<!-- Google Code para etiquetas de remarketing -->
    <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 832570693;
      var google_custom_params = window.google_tag_params; var google_remarketing_only = true;
      /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
      <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/832570693/?guid=ON&amp;script=0"/>
      </div>
    </noscript>

</body>
</html>