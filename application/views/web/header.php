<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <meta property="og:url"           content="<?php echo site_url('web/index') ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="PRIMAX TE REGALA UN AÑO DE GASOLINA GRATIS" />
    <meta property="og:description"   content="¡PRIMAX está sorteando un año de gasolina gratis! Si me lo gano, me iré a broncearme a la playa todos los fines de semana ¿Quién me acompaña? " />
    <meta property="og:image"         content="<?php echo base_url('public/img/facebook/facebook-01.png'); ?>" />
    <?php if ($this->router->method == "form_process"): ?>
    <?php endif; ?>
    
  	<title><?php echo $titulo; ?></title>
  	<link href="<?php echo base_url('public/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
  	<link href="<?php echo base_url('public/css/custom-bootstrap-margin-padding.css'); ?>" rel="stylesheet" type="text/css">
  	<link href="<?php echo base_url('public/css/fonts.css'); ?>" rel="stylesheet" type="text/css">
  	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  	<link href="<?php echo base_url('public/css/icon-redes.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('public/css/misc.css'); ?>" rel="stylesheet" type="text/css">
  	

    <link href="<?php echo base_url('public/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('public/css/bootstrap-social.css'); ?>" rel="stylesheet" type="text/css">
  
    <link href="<?php echo base_url('public/css/estilo.css'); ?>" rel="stylesheet" type="text/css">
	
    <?php if ($this->router->method == 'index'): ?>
  		<link href="<?php echo base_url('public/css/index.css'); ?>" rel="stylesheet" type="text/css">
  	<?php elseif ($this->router->method == 'exito'): ?>
  		<link href="<?php echo base_url('public/css/exito.css'); ?>" rel="stylesheet" type="text/css">
  	<?php elseif ($this->router->method == 'form_process'): ?>
  		<link href="<?php echo base_url('public/css/form.css'); ?>" rel="stylesheet" type="text/css">
  	<?php endif; ?>

  	<script type="text/javascript">
          var base_url = '<?php echo base_url(); ?>';

          var js_site_url = function( urlText ){
              var urlTmp = "<?php echo site_url('" + urlText + "'); ?>";
              return urlTmp;
          }

          var js_base_url = function( urlText ){
              var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
              return urlTmp;
          }
      </script>

      <link rel="icon" type="image/jpeg" href="<?php echo base_url('public/img/favicon.jpg'); ?>" />



    <!-- Facebook Pixel Code -->
    <script type="text/javascript">
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '143002769643023');
      fbq('track', 'PageView');
    </script>
    <noscript>
      <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=143002769643023&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->


    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107280953-1"></script>
    <script type="text/javascript">
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments)};
      gtag('js', new Date());
      gtag('config', 'UA-107280953-1');
    </script>
  </head>
  <?php if ($this->router->method == 'exito'): ?>
    <?php $claseFondo = "fondo-sin-textura" ?>
  <?php else: ?>
    <?php $claseFondo = "fondo-textura" ?>
  <?php endif; ?>
  <body class="<?php echo $claseFondo; ?>">
  	<?php if ($this->router->method == 'index') {
  		$variablefixed = "navbar-fixed-top";
  	}else{	
  		$variablefixed = "";
  	} ?>
    <nav class="navbar <?php echo $variablefixed; ?> hidden-xs hidden-sm">
      <div class="container-fluid mt-20">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo site_url('web/index') ?>">
          	<div class="div-logo-horizontal">
          		<img class="img-responsive" src="<?php echo base_url('public/svg/logo-horizontal.svg') ?>" alt="Primax" />
          	</div>
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right mt-15">
            <li>
            	<a href="https://www.facebook.com/PrimaxEcuador/" class="icon-redes-wrapper" target="_blank"><span class="icon-redes-fb icon-redes"></span></a>
            </li>
            <li>
            	<a href="https://www.instagram.com/primaxec/" class="icon-redes-wrapper" target="_blank"><span class="icon-redes-ig icon-redes"></span></a>
            </li>
            <li>
            	<a href="https://www.youtube.com/channel/UCim4K4o2nv2bRrtESHdk-dg" class="icon-redes-wrapper" target="_blank"><span class="icon-redes-yt icon-redes"></span></a>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <nav class="navbar <?php echo $variablefixed; ?> hidden-lg hidden-md">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo site_url('web/index') ?>">
            <div class="div-logo-horizontal-movil">
              <img class="img-responsive" src="<?php echo base_url('public/svg/logo-horizontal.svg') ?>" alt="Primax" />
            </div>
          </a>
        </div>
      </div>
    </nav>

    <!-- inicio #wrapper -->
	 <div class="container-fluid">