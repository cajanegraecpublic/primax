			<script>
			  var meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
			  // This is called with the results from from FB.getLoginStatus().
			  function statusChangeCallback(response) {
			    console.log('statusChangeCallback');
			    console.log(response);
			    // The response object is returned with a status field that lets the
			    // app know the current login status of the person.
			    // Full docs on the response object can be found in the documentation
			    // for FB.getLoginStatus().
			    if (response.status === 'connected') {
			      // Logged into your app and Facebook.
			      testAPI();
			    } else {
			      // The person is not logged into your app or we are unable to tell.
			    }
			  }

			  // This function is called when someone finishes with the Login
			  // Button.  See the onlogin handler attached to it in the sample
			  // code below.
			  function checkLoginState() {
			    FB.getLoginStatus(function(response) {
			      statusChangeCallback(response);
			    });
			  }

			  window.fbAsyncInit = function() {
				  FB.init({
				    appId      : '131098367531778',
				    cookie     : true,  // enable cookies to allow the server to access 
				                        // the session
				    xfbml      : true,  // parse social plugins on this page
				    version    : 'v2.10' // use graph api version 2.8
				  });

				  // Now that we've initialized the JavaScript SDK, we call 
				  // FB.getLoginStatus().  This function gets the state of the
				  // person visiting this page and can return one of three states to
				  // the callback you provide.  They can be:
				  //
				  // 1. Logged into your app ('connected')
				  // 2. Logged into Facebook, but not your app ('not_authorized')
				  // 3. Not logged into Facebook and can't tell if they are logged into
				  //    your app or not.
				  //
				  // These three cases are handled in the callback function.

				  FB.getLoginStatus(function(response) {
				    statusChangeCallback(response);
				  });

			  };

				(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=131098367531778";
				fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));

			  // Load the SDK asynchronously
			  // (function(d, s, id) {
			  //   var js, fjs = d.getElementsByTagName(s)[0];
			  //   if (d.getElementById(id)) return;
			  //   js = d.createElement(s); js.id = id;
			  //   js.src = "https://connect.facebook.net/en_US/sdk.js";
			  //   fjs.parentNode.insertBefore(js, fjs);
			  // }(document, 'script', 'facebook-jssdk'));

			  // Here we run a very simple test of the Graph API after login is
			  // successful.  See statusChangeCallback() for when this call is made.
			  function testAPI() {
			    console.log('Welcome!  Fetching your information.... ');
			    FB.api('me?fields=id,name,first_name,last_name,email,birthday,location,hometown', function(response) {
			      console.log(response);
			      $("#user-name").val(response.first_name);
				  $("#user-lastname").val(response.last_name);
				  $("#user-mail").val(response.email);
				  var birth_date = response.birthday.split("/");
				  $("#user-birth-day").val(birth_date[1]);
				  $("#user-birth-month").val(meses[parseInt(birth_date[0])]);
				  $("#user-birth-year").val(birth_date[2]);
				  var location = response.location.name.split(", ");
				  var city = location[0];
				  $("#user-city").val(city);
			      console.log('Successful login for: ' + response.name);
			      console.log('Birthday: ' + response.birthday);
			      console.log('City: ' + city);
			    });
			  }


			function validarCedula(cedula) {
				//Preguntamos si la cedula consta de 10 digitos
				if (cedula.length == 10) {
					//Obtenemos el digito de la region que sonlos dos primeros digitos
					var digito_region = cedula.substring(0, 2);

					//Pregunto si la region existe ecuador se divide en 24 regiones
			        if (digito_region >= 1 && digito_region <= 24)  {
			        	// Extraigo el ultimo digito
			        	var ultimo_digito = cedula.substring(9, 10);

			        	//Agrupo todos los pares y los sumo
			        	var pares = parseInt(cedula.substring(1, 2)) + parseInt(cedula.substring(3, 4)) + parseInt(cedula.substring(5, 6)) + parseInt(cedula.substring(7, 8));

			        	//Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
			        	var numero1 = cedula.substring(0, 1);
			        	var numero1 = (numero1 * 2);
			        	if (numero1 > 9) {
			        		var numero1 = (numero1 - 9);
			        	}

			        	var numero3 = cedula.substring(2, 3);
			        	var numero3 = (numero3 * 2);
			        	if (numero3 > 9) {
			        		var numero3 = (numero3 - 9);
			        	}

			        	var numero5 = cedula.substring(4, 5);
			        	var numero5 = (numero5 * 2);
			        	if (numero5 > 9) {
			        		var numero5 = (numero5 - 9);
			        	}

			        	var numero7 = cedula.substring(6, 7);
			        	var numero7 = (numero7 * 2);
			        	if (numero7 > 9) {
			        		var numero7 = (numero7 - 9);
			        	}

			        	var numero9 = cedula.substring(8, 9);
			        	var numero9 = (numero9 * 2);
			        	if (numero9 > 9) {
			        		var numero9 = (numero9 - 9);
			        	}

			        	var impares = numero1 + numero3 + numero5 + numero7 + numero9;

			        	//Suma total
			        	var suma_total = (pares + impares);

			        	//extraemos el primero digito
			        	var primer_digito_suma = String(suma_total).substring(0, 1);

			        	//Obtenemos la decena inmediata
			        	var decena = (parseInt(primer_digito_suma) + 1)  * 10;

			        	//Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
			        	var digito_validador = decena - suma_total;

			        	//Si el digito validador es = a 10 toma el valor de 0
			        	if (digito_validador == 10) {
			        		var digito_validador = 0;
			        	}

			        	//Validamos que el digito validador sea igual al de la cedula
			        	if (digito_validador == ultimo_digito) {
			        		console.log('Cédula Válida');
			        		return true;
			        	} else {
			        		console.log('Cédula No válida');
			        		return false;
			        	}
			        } else {
			        	// imprimimos en consola si la region no pertenece
			        	console.log('Cédula incorrecta');
			        	return false;
			        }
			    } else {
			    	//imprimimos en consola si la cedula tiene mas o menos de 10 digitos
			    	console.log('Cédula No válida (Deben ser 10 numéros)');
			        return false;
			    }
			}
			</script>

			<!--
			  Below we include the Login Button social plugin. This button uses
			  the JavaScript SDK to present a graphical Login button that triggers
			  the FB.login() function when clicked.
			-->
			<div class="row mb-20">
				<div class="col-lg-3 col-lg-offset-2 col-md-4 col-xs-12">
					<div id="form-header">
						<p class="paso-label-mobile">
							INGRESA
							<br />
							TUS DATOS
						</p>
					</div>
					<div class="row mt-20">
						<div class="col-xs-6 col-md-7">
							<h4 class="txt-azul font-clio-black mt-5 mb-0">
								Agilita tu registro
							</h4>
						</div>
						<div class="col-xs-6 col-md-5">
							<fb:login-button id="form-fb-login" class="pr-0" scope="public_profile,email,user_birthday,user_location,user_hometown" onlogin="checkLoginState();"></fb:login-button>
						</div>
					</div>
					<div id="form-body" class="mt-20">
						<?php echo form_open('form/form_process' , array('id' => 'form-register', 'enctype' => 'multipart/form-data')); ?>
							<div class="form-group">
								<?php echo form_input(array(
									'id' => 'user-name',
									'name' => 'user-name',
									'value' => '',
									'placeholder' => 'Nombre',
									'class' => 'form-control user-input inputPrimax',
									'required' => true
								));?>
								<?php echo form_error('user-name','<span class="help-block">','</span>'); ?>
							</div>
							<div class="form-group">
								<?php echo form_input(array(
									'id' => 'user-lastname',
									'name' => 'user-lastname',
									'value' => '',
									'placeholder' => 'Apellido',
									'class' => 'form-control user-input inputPrimax',
									'required' => true
								));?>
								<?php echo form_error('user-lastname','<span class="help-block">','</span>'); ?>
							</div>
							<div class="form-group">
								<?php echo form_input(array(
									'id' => 'user-mail',
									'name' => 'user-mail',
									'value' => '',
									'placeholder' => 'Mail',
									'class' => 'form-control user-input inputPrimax',
									'required' => true
								));?>
								<?php echo form_error('user-mail','<span class="help-block">','</span>'); ?>
							</div>
							<div class="form-group">
								<?php echo form_input(array(
									'id' => 'user-phone',
									'name' => 'user-phone',
									'value' => '',
									'placeholder' => 'Celular',
									'class' => 'form-control user-input inputPrimax',
									'required' => true
								));?>
								<?php echo form_error('user-phone','<span class="help-block">','</span>'); ?>
							</div>
							<div class="form-group">
								<?php echo form_input(array(
									'id' => 'user-id',
									'name' => 'user-id',
									'value' => '',
									'placeholder' => 'Cédula',
									'class' => 'form-control user-input inputPrimax',
									'required' => true
								));?>
								<?php echo form_error('user-id','<span class="help-block">','</span>'); ?>
							</div>
							<div class="form-group">
								<label for="">Fecha de nacimiento</label>
								<div class="form-inline">
									<select class="form-control" required name="user-birth-day" id="user-birth-day" form="form-register">
										<option selected disabled hidden>día</option>
									</select>
									<select class="form-control" required name="user-birth-month" id="user-birth-month" form="form-register">
										<option selected disabled hidden>mes</option>
									</select>
									<select class="form-control" required name="user-birth-year" id="user-birth-year" form="form-register">
										<option selected disabled hidden>año</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="user-city">Ciudad</label>
								<select required name="user-city" id="user-city" form="form-register" class="form-control">
									<option selected disabled hidden>ciudad</option>
									<?php foreach ($ciudades as $ciudad): ?>
										<option><?php echo $ciudad->nombre; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-12 col-xs-12 text-center">
										<button type="button" class="btn btn-lg btn-primax-2 hidden-sm hidden-xs" data-toggle="modal" data-target="#myModal" id="btnAdjuntarWeb">
											<img class="img-responsive img-center img-adjuntar" src="<?php echo base_url('public/img/boton-adjuntar.png'); ?>" alt="" />
										</button>
										<button type="button" class="btn btn-lg btn-primax-2 hidden-lg hidden-md" data-toggle="modal" data-target="#myModalMobile" id="btnAdjuntarMovil">
											<img class="img-responsive img-center img-adjuntar" src="<?php echo base_url('public/img/boton-adjuntar.png'); ?>" alt="" />
										</button>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-xs-12 pl-0 pr-0">
										<h5 class="text-uppercase text-center mt-0 mb-0 font-10">
											No olvides guardar tu factura para canjear el premio
										</h5>
									</div>
								</div>
							</div>

							<div class="form-inline">
								<div class="row mt-30 hidden-lg hidden-md">
									<div class="col-xs-12">
										<input id="user-bill" class="user-bill" name="user-bill" type="file" accept="image/*;capture=camera" />
									</div>
								</div>
								<div class="row mt-20">
									<div class="col-lg-2 col-xs-2 contenedor-centrarP text-right pr-5">
										<input id="user-terms-accept" type="checkbox" name="user-terms-accept">
									</div>
									<div class="col-lg-10 col-xs-10 contenedor-centrarP pl-0">
										<label id="user-terms-accept-label" class="text-amiko-regular" for="user-terms-accept">
											ACEPTO LOS <a href="<?php echo base_url('public/docs/terminos.pdf'); ?>" target="_blank" class="text-sub text-amiko-bold">TÉRMINOS Y CONDICIONES</a>
										</label>
										<?php echo form_error('user-terms-accept','<span class="help-block">','</span>'); ?>
									</div>
								</div>
								<div class="hidden"><?php phpinfo(); ?></div>
								<div class="row mt-20">
									<div class="col-lg-12 text-center">
										<div class="form-group">
											<?php echo form_input(array(
												'id' => 'form-submit',
												'name' => 'form-submit',
												'value' => 'Enviar',
												'class' => 'form-control',
												'type' => 'submit'
											));?>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
				<div class="col-lg-4 col-lg-offset-1 col-xs-5 col-xs-offset-1 hidden-xs hidden-sm">
					<div class="row">
						<div class="col-xs-11 col-xs-offset-1">
							<img class="img-responsive img-center" src="<?php echo base_url('public/img/logo_promo2.png'); ?>" alt="¡1 año de gasolina gratis!">							
						</div>
					</div>
					<div class="row mt-20">
						<div class="col-lg-11 col-lg-offset-1 text-center">
							<h3 class="txt-cio-ultrablack mt-0 mb-0">
								<span class="txt-gris">QUINTO SORTEO</span>
								<span class="txt-naranja">11.DIC.2017</span>
							</h3>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
			<div class="row">
				<div id="lower-container" class="col-xs-12 mb-10 text-center terms-and-conditions">
					<a href="<?php echo base_url('public/docs/terminos.pdf'); ?>" target="_blank">Conoce los términos y condiciones del concurso</a> | &copy; 2017 Primax Ecuador. Todos los derechos reservados
				</div>
			</div>


			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content modal-naranja">
			      <div class="modal-body">
			        <div class="row">
			        	<button type="button" class="close modal-cerrar" data-dismiss="modal" aria-label="Close">
			        		<span aria-hidden="true">&times;</span>
			        	</button>
			        	<img class="img-responsive" src="<?php echo base_url('public/img/modalfactura.png'); ?>" alt="">
			        </div>
			      </div>
			    </div>
			  </div>
			</div>


			<div class="modal fade" id="myModalMobile" tabindex="-1" role="dialog" aria-labelledby="myModalMobileLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content modal-naranja">
			      <div class="modal-body">
			        <div class="row">
			        	<button type="button" class="close modal-cerrar" data-dismiss="modal" aria-label="Close">
			        		<span aria-hidden="true">&times;</span>
			        	</button>
			        	<img class="img-responsive" src="<?php echo base_url('public/img/modalfacturaMobile.png'); ?>" alt="">
			        </div>
			      </div>
			    </div>
			  </div>
			</div>