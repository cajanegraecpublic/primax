<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Admin_user extends CI_Model{
        private $username = '';
		private $name = '';
        private $email = '';
        private $password = '';
        
        function __construct() {
            parent::__construct();
            $this->load->database();
            $this->load->library('session');
        }

        ///////////////////////////////////
        // Getters
        ///////////////////////////////////
        public function get_username()
        {
            return $this->username;
        }

        public function get_name()
        {
            return $this->name;
        }

        public function get_email()
        {
            return $this->email;
        }

        public function get_password()
        {
            return $this->password;
        }

        ///////////////////////////////////
        // Setters
        ///////////////////////////////////
        public function set_username($username)
        {
            $this->username = $username;
        }

        public function set_name($name)
        {
            $this->name = $name;
        }

        public function set_email($email)
        {
            $this->email = $email;
        }

        public function set_password($password)
        {
            $this->password = $password;
        }

        ///////////////////////////////////
        // Métodos
        ///////////////////////////////////
        // Método para el inicio de sesión de un usuario administrador
        public static function login($username, $password){
            // Obtenemos instancia de CodeIgniter para manejo de la DB
            $instancia_ci =& get_instance();

        	$admin_user = $instancia_ci->db->get_where('admin', array('user'=> $username , 'password' => md5($password)))->row();

        	if ($admin_user) {
                $data_user = array(
                    'type' => 'admin',
                    'username' => $admin_user->user,
                    'name' => $admin_user->persona,
                    'email' => $admin_user->correo
                );

                // Guardamos en sesión los datos del usuario administrador
                $instancia_ci->session->set_userdata($data_user);

                return true;
            } else {
                return false;
            }
        }

        // Método para cerrar la sesión de un usuario administrador
        public static function logout()
        {   
            // Obtenemos instancia de CodeIgniter para manejo de la DB
            $instancia_ci =& get_instance();
            
        	$instancia_ci->session->sess_destroy();
        }
	}
?>