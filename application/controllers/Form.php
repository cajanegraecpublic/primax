<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('image_moo');
		date_default_timezone_set('America/Guayaquil');
        $this->load->library('session');
	}

    public function form_process()
    {
        // Redirigir al home porque se acabó la promoción
        redirect('web/index');

        //////////////////////
        $file_upload_error_msg = '';

        if ($this->input->post('form-submit')) {
            $this->form_validation->set_rules('user-name', 'Nombre', 'required|callback_name_check');
            $this->form_validation->set_rules('user-lastname', 'Apellido', 'required|callback_name_check');
            $this->form_validation->set_rules('user-mail', 'Email', 'required|callback_email_check');
            $this->form_validation->set_rules('user-phone', 'Celular', 'required|callback_celular_check');
            $this->form_validation->set_rules('user-id', 'Cédula', 'required|callback_cedula_check');
            $this->form_validation->set_rules('user-birth-day', 'Día', 'required|callback_day_check');
            $this->form_validation->set_rules('user-birth-month', 'Mes', 'required|callback_month_check');
            $this->form_validation->set_rules('user-birth-year', 'Año', 'required|callback_year_check');
            $this->form_validation->set_rules('user-city', 'Ciudad', 'required|callback_city_check');
            $this->form_validation->set_rules('user-terms-accept', 'Términos', 'required');

            if($this->form_validation->run() == true) {
                // Configuración de uploads
                $config['upload_path'] = 'assets/facturas';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size'] = '10240';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);

                $factura = '';
                // Validamos si el usuario envió una factura
                if ($this->upload->do_upload('user-bill')) {
                    $img_data = $this->upload->data();
                    $factura = $img_data['file_name'];
                    $form_data = $this->input->post();

                    if ($factura != '' && $form_data) {
                        $ruta = 'assets/facturas';
                        $this->load->library('image_moo');
                        $archivoImagen = $ruta .'/' . $factura;
                        $this->image_moo
                            ->load( $archivoImagen )
                            ->resize( 500, FALSE, TRUE)
                            ->set_jpeg_quality(95)
                            ->save( $archivoImagen, TRUE );

                        // Borramos exceso de espacios vacios en el string del nombre
                        $nombre = $form_data['user-name'];
                        $nombre = trim($nombre);
                        $nombre = preg_replace('/\s+/', ' ', $nombre);

                        // Borramos exceso de espacios vacios en el string del apellido
                        $apellido = $form_data['user-lastname'];
                        $apellido = trim($apellido);
                        $apellido = preg_replace('/\s+/', ' ', $apellido);

                        $correo = $form_data['user-mail'];

                        // Borramos exceso de espacios vacios en el string del celular
                        $celular = $form_data['user-phone'];
                        $celular = trim($celular);
                        $celular = preg_replace('/\s+/', '', $celular);

                        // Borramos exceso de espacios vacios en el string del número de cédula
                        $cedula = $form_data['user-id'];
                        $cedula = trim($cedula);
                        $cedula = preg_replace('/\s+/', '', $cedula);

                        // Borramos exceso de espacios vacios en el string del día de nacimiento
                        $dia = $form_data['user-birth-day'];
                        $dia = trim($dia);
                        $dia = preg_replace('/\s+/', '', $dia);

                        // Borramos exceso de espacios vacios en el string del mes de nacimiento
                        $mes = $form_data['user-birth-month'];
                        $mes = trim($mes);
                        $mes = preg_replace('/\s+/', '', $mes);
                        $meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                        $mes = array_search($mes, $meses);

                        // Borramos exceso de espacios vacios en el string del año de nacimiento
                        $ano = $form_data['user-birth-year'];
                        $ano = trim($ano);
                        $ano = preg_replace('/\s+/', '', $ano);

                        // Borramos exceso de espacios vacios en el string de la ciudad
                        $ciudad = $form_data['user-city'];
                        $ciudad = trim($ciudad);
                        $ciudad = preg_replace('/\s+/', ' ', $ciudad);
                        $ciudad = $this->db->get_where('geo_ciudad', array('nombre' => $ciudad))->row();
                        
                        $fecha_nacimiento = $ano . str_pad($mes, 2, '0', STR_PAD_LEFT) . str_pad($dia, 2, '0', STR_PAD_LEFT);
                        $data = array(
                            'nombre' => $nombre,
                            'apellido' => $apellido,
                            'cedula' => $cedula,
                            'celular' => $celular,
                            'correo' => $correo,
                            'fecha_nacimiento' => $fecha_nacimiento,
                            'id_ciudad' => $ciudad->id,
                            'ingresado' => time(),
                            'factura' => $factura,
                            'fb_share' => 0,
                            'estado' => 1
                        );
                        $this->db->insert('participaciones', $data);
                        $part = $this->db->get_where("participaciones", ["cedula" => $cedula, "ingresado" => time()])->row();
                        $this->session->set_userdata("id_user", $part->id);
                        $this->session->set_userdata("correo", 1);
                        $this->session->set_userdata("compartir", 1);
                        redirect('web/exito');
                    } else {
                        $file_upload_error_msg =  'Error. Falla de formdata o factura.';
                    }
                } else {
                    $file_upload_error_msg = 'Error. No ha enviado la foto de la factura.';
                }
            } else {
                $file_upload_error_msg =  'Error. Debe llenar bien todos los campos.';
            }
        } else {
            $file_upload_error_msg =  'Error. No ha enviado la foto de la factura.';
        }

        // Recuperamos todas las ciudades de la DB
        $this->db->select('nombre');
        $this->db->from('geo_ciudad');
        $this->db->order_by('nombre', 'ASC');
        $ciudades_arr = $this->db->get()->result();

        $data_header['titulo'] = 'Primax - 1 año de gasolina gratis';
        $data_body['file_upload_error_msg'] = $file_upload_error_msg;
        $data_body['ciudades'] = $ciudades_arr;
        $this->load->view('web/header', $data_header);
        $this->load->view('web/form', $data_body);
        $this->load->view('web/footer');
    }

    public function complete_fill_in() {
        $response = array("codigo" => 0, "mensaje" => "");
        if ($this->session->has_userdata("id_user") && $this->session->has_userdata("compartir") && $this->session->userdata("compartir") == 1) {
            $participante = $this->db->get_where("participaciones", ["id" => $this->session->userdata("id_user")])->row();
            if ($participante) {
                $data = array(
                    'nombre' => $participante->nombre,
                    'apellido' => $participante->apellido,
                    'cedula' => $participante->cedula,
                    'celular' => $participante->celular,
                    'correo' => $participante->correo,
                    'fecha_nacimiento' => $participante->fecha_nacimiento,
                    'id_ciudad' => $participante->id_ciudad,
                    'ingresado' => $participante->ingresado,
                    'factura' => $participante->factura,
                    'fb_share' => 1,
                    'estado' => 1
                );
                $this->db->insert('participaciones', $data);
                $response["codigo"] = 1;
                $response["mensaje"] = "éxito";
            } else {
                $response["codigo"] = 0;
                $response["mensaje"] = "no encontró participante con ese id";
            }
            $this->session->unset_userdata('id_user');
            $this->session->unset_userdata("compartir");
        } else {
            $response["codigo"] = 0;
            $response["mensaje"] = "no encontró id en sesión";
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function name_check($user_name)
    {
        if ($user_name != ' ' && preg_match("/^[\p{L} '-]+$/u", $user_name)) {
            return true;
        } else {
            $this->form_validation->set_message('name_check', 'Nombre/Apellido no válido.');
            return false;
        }
    }

    public function email_check($user_email)
    {
        if ($user_email != ' ' && preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $user_email)) {
            return true;
        } else {
            $this->form_validation->set_message('email_check', 'Correo no válido.');
            return false;
        }
    }

    public function celular_check($user_phone)
    {
        if (!is_numeric($user_phone)) {
            $this->form_validation->set_message('celular_check', 'Celular no válido (10 digitos).');
            return false;
        } elseif (strlen($user_phone) != 10) {
            $this->form_validation->set_message('celular_check', 'Celular no válido (10 digitos).');
            return false;
        } else {
            return true;
        }
    }

    public function cedula_check($user_id)
    {
        if (!is_numeric($user_id)) {
            $this->form_validation->set_message('cedula_check', 'Número de cédula no válido (10 dígitos).');
            return false;
        } elseif (strlen($user_id) != 10) {
            $this->form_validation->set_message('cedula_check', 'Número de cédula no válido (10 dígitos).');
            return false;
        } elseif (!$this->validar_cedula($user_id)) {
            return false;
        } else {
            return true;
        }
    }

    public function day_check($user_birth_day)
    {
        if (!is_numeric($user_birth_day)) {
            $this->form_validation->set_message('day_check', 'Día de nacimiento no válido. Debe ser número del 1 al 31.');
            return false;
        } elseif (strlen($user_birth_day) != 1 && strlen($user_birth_day) != 2) {
            $this->form_validation->set_message('day_check', 'Día de nacimiento no válido. Debe ser número del 1 al 31.');
            return false;
        } elseif ($user_birth_day < 1 || $user_birth_day > 31) {
            $this->form_validation->set_message('day_check', 'Día de nacimiento no válido. Debe ser número del 1 al 31.');
            return false;
        } else {
            return true;
        }
    }

    public function month_check($user_birth_month)
    {
        if ($user_birth_month != ' ' && preg_match("/^[a-zA-Z]+$/", $user_birth_month)) {
            $meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
            $found = array_search($user_birth_month, $meses);

            if (!$found) {
                $this->form_validation->set_message('month_check', 'Mes de nacimiento no válido.');
                return false;
            } else {
                return true;
            }
        } else {
            $this->form_validation->set_message('month_check', 'Mes de nacimiento no válido.');
            return false;
        }
    }

    public function year_check($user_birth_year)
    {
        if (!is_numeric($user_birth_year)) {
            $this->form_validation->set_message('year_check', 'Año de nacimiento no válido. Debe ser número entre 1900 y 2020.');
            return false;
        } elseif (strlen($user_birth_year) != 4) {
            $this->form_validation->set_message('year_check', 'Año de nacimiento no válido. Debe ser número entre 1900 y 2020.');
            return false;
        } elseif ($user_birth_year < 1900 || $user_birth_year > 2020) {
            $this->form_validation->set_message('year_check', 'Año de nacimiento no válido. Debe ser número entre 1900 y 2020.');
            return false;
        } else {
            return true;
        }
    }

    public function city_check($user_city)
    {
        if ($user_city != ' ' && preg_match("/^[\p{L} '-]+$/u", $user_city)) {
            $ciudad = $this->db->get_where('geo_ciudad', array('nombre' => $user_city))->row();

            if ($ciudad) {
                return true;
            } else {
                $this->form_validation->set_message('city_check', 'Ciudad no válida.');
                return false;
            }
        } else {
            $this->form_validation->set_message('city_check', 'Ciudad no válida.');
            return false;
        }
    }

    private function validar_cedula($cedula)
    {
        //Preguntamos si la cedula consta de 10 digitos
        if (strlen($cedula) == 10) {

            //Obtenemos el digito de la region que sonlos dos primeros digitos
            $digito_region = substr($cedula, 0, 2);

            //Pregunto si la region existe ecuador se divide en 24 regiones
            if ($digito_region >= 1 && $digito_region <=24 ) {
                // Extraigo el ultimo digito
                $ultimo_digito = substr($cedula, 9, 1);

                //Agrupo todos los pares y los sumo
                $pares = intval(substr($cedula, 1, 1)) + intval(substr($cedula, 3, 1)) + intval(substr($cedula, 5, 1)) + intval(substr($cedula, 7, 1));

                //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
                $numero1 = intval(substr($cedula, 0, 1));
                $numero1 = ($numero1 * 2);
                if ($numero1 > 9) {
                    $numero1 = ($numero1 - 9);
                }

                $numero3 = intval(substr($cedula, 2, 1));
                $numero3 = ($numero3 * 2);
                if ($numero3 > 9) {
                    $numero3 = ($numero3 - 9);
                }

                $numero5 = intval(substr($cedula, 4, 1));
                $numero5 = ($numero5 * 2);
                if ($numero5 > 9) {
                    $numero5 = ($numero5 - 9);
                }

                $numero7 = intval(substr($cedula, 6, 1));
                $numero7 = ($numero7 * 2);
                if ($numero7 > 9) {
                    $numero7 = ($numero7 - 9);
                }

                $numero9 = intval(substr($cedula, 8, 1));
                $numero9 = ($numero9 * 2);
                if ($numero9 > 9) {
                    $numero9 = ($numero9 - 9);
                }

                $impares = $numero1 + $numero3 + $numero5 + $numero7 + $numero9;

                //Suma total
                $suma_total = ($pares + $impares);

                //extraemos el primero digito
                $primer_digito_suma = substr("" . $suma_total, 0, 1);

                //Obtenemos la decena inmediata
                $decena = (intval($primer_digito_suma) + 1) * 10;

                //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
                $digito_validador = $decena - $suma_total;

                //Si el digito validador es = a 10 toma el valor de 0
                if ($digito_validador == 10) {
                    $digito_validador = 0;
                }

                //Validamos que el digito validador sea igual al de la cedula
                if ($digito_validador == $ultimo_digito) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}