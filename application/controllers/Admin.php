<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('Admin_user');
        date_default_timezone_set('America/Guayaquil');
    }

    public function administrar_participaciones()
    {
        if ($this->security_check_admin()) {
            $data_header['titulo'] = 'Primax - Admin - Inicio';

            $participantes = Admin::get_participantes("", "");

            $this->load->view('admin/header', $data_header);
            $this->load->view('admin/lat-menu');
            $this->load->view('admin/administrar_participaciones', ['participantes' => $participantes]);
            $this->load->view('admin/footer');
        } else {
            redirect('admin/login');
        }
    }

    public function auth()
    {
        $username = $this->input->post('user');
        $password = $this->input->post('password');

        Admin_user::login($username, $password);

        if ($this->session->userdata('user') != '' && $this->session->userdata('tipo') == 'admin') {
            redirect('admin/index');
        } else {
            redirect('admin/login');
        }
    }

    public function login()
    {
        if ($this->security_check_admin()) {
            redirect('admin/index');
        } else {
            $data_header['titulo'] = 'Primax - Admin - Login';

            $this->load->view('admin/header', $data_header);
            $this->load->view('admin/login');
            $this->load->view('admin/footer');
        }
    }

    public function logout()
    {
        if($this->security_check_admin()){
            Admin_user::logout();

            redirect('admin/login');
        }else{
            redirect('admin/login');
        }
    }

    public function index()
    {
        if ($this->security_check_admin()) {
            $data_header['titulo'] = 'Primax - Admin - Inicio';

            $this->load->view('admin/header', $data_header);
            $this->load->view('admin/lat-menu');
            $this->load->view('admin/index');
            $this->load->view('admin/footer');
        } else {
            redirect('admin/login');
        }
    }

    private function security_check_admin() {
        $admin_user = $this->session->userdata('username');

        if ($admin_user == '') {
            return false;
        } else {
            if ($this->session->userdata('type') == 'admin') {

                return true;
            } else {
                Admin_user::logout();

                return false;
            }
        }
    }

    public static function get_participantes($fecha_inicio, $fecha_fin) {
        $instanciaCI =& get_instance();

        $instanciaCI->db->where("estado", 2);
        $instanciaCI->db->update("participaciones", ["estado" => 1]);

        $instanciaCI->db->select('p.*, c.nombre as ciudad');
        $instanciaCI->db->from('participaciones as p');
        $instanciaCI->db->join('geo_ciudad as c', 'p.id_ciudad = c.id');
        $instanciaCI->db->where("estado", 1);
        if ($fecha_inicio != '') {
            $instanciaCI->db->where('ingresado >=', strtotime($fecha_inicio));
        }
        if ($fecha_fin != '') {
            $instanciaCI->db->where('ingresado <=', strtotime($fecha_fin));
        }
        $participantes = $instanciaCI->db->get()->result_array();

        foreach ($participantes as $participante) {
            $instanciaCI->db->where("id", $participante["id"]);
            $instanciaCI->db->update("participaciones", ["estado" => 2]);
        }

        return $participantes;
    }

    public static function eliminarDatos() {
        $instanciaCI =& get_instance();

        $instanciaCI->db->where("estado", 2);
        $instanciaCI->db->update("participaciones", ["estado" => 0]);

        $data = ["codigo" => 1];

        header('Content-type: application/json');
        echo json_encode($data);

        return true;
    }
}