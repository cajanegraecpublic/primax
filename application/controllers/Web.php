<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('form');
		date_default_timezone_set('America/Guayaquil');
    $this->load->library('session');
  }

  	public function index() {
    $data_header['titulo'] = 'Primax - 1 año de gasolina gratis';
    $this->load->view('web/header', $data_header);
    $this->load->view('web/index');
    $this->load->view('web/footer');
    }

    public function exito() {
  		$data_header['titulo'] = 'Primax - 1 año de gasolina gratis';
  		$this->load->view('web/header', $data_header);
  		$this->load->view('web/exito');
  		$this->load->view('web/footer');
  	}

	public function form() {
		// Recuperamos todas las ciudades de la DB
		$this->db->select('nombre');
		$this->db->from('geo_ciudad');
		$this->db->order_by('nombre', 'ASC');
		$ciudades_arr = $this->db->get()->result();

		$data_header['titulo'] = 'Primax - 1 año de gasolina gratis';
		$data_body['ciudades'] = $ciudades_arr;
		$this->load->view('web/header', $data_header);
		$this->load->view('web/form', $data_body);
		$this->load->view('web/footer');
	}

  public function sendMail() {
      $response = array("codigo" => 0, "mensaje" => "");
      if ($this->session->has_userdata("id_user") && $this->session->has_userdata("correo")) {
          // Obtener instancia de CodeIgniter
          // $instanciaCI =& get_instance();
          $participante = $this->db->get_where("participaciones", ["id" => $this->session->userdata("id_user")])->row();
          if ($participante) {
              $correo = $participante->correo;
              try {

                  /////////////////////////////////////////////////////////////////////////
                  // Preparación de correo electrónico
                  $this->load->library('email');

                  $correo_config = $this->db->get_where('correo_config', array('estado'=>1))->row();
                  $config['protocol']  = 'smtp';
                  $config['smtp_host'] = $correo_config->servidor_correo; // URL DEL HOST SMTP
                  $config['smtp_port'] = $correo_config->puerto; // PUERTO SMTP DEL CLIENTE (va sin comillas)
                  $config['smtp_user'] = $correo_config->email; // CORREO SMTP
                  $config['smtp_pass'] = $correo_config->password; // CONTRASEÑA DEL EMAIL
                  $config['charset']   = 'utf-8';
                  $config['mailtype']  = 'html';

                  $this->email->initialize($config);
                  $this->email->set_newline("\r\n");
                  $this->email->set_crlf("\r\n");

                  // $opts = array(
                  //     'http' => array(
                  //         'method'  => 'POST',
                  //         'header'  => 'Content-type: application/x-www-form-urlencoded',
                  //         'content' => http_build_query( $mailPostData )
                  //     )
                  // );
                  // $cuerpoMail = file_get_contents( base_url( $mailTemplateURL ), false, stream_context_create( $opts ) );

                  $this->email->from('info@supergprix.com', 'PRIMAX ECUADOR');
                  $this->email->to( $correo );
                  if( isset( $destinatariosData["cc"] ) && !empty( $destinatariosData["cc"] ) ){
                      $this->email->cc( $destinatariosData["cc"] );
                  }
                  if( isset( $destinatariosData["bcc"] ) && !empty( $destinatariosData["bcc"] ) ){
                      $this->email->bcc( $destinatariosData["bcc"] );
                  }
                  $this->email->subject( "¡Cool! ya estas participando por un año de gasolina gratis." );
                  $this->email->message( "<div style=\"max-width: 1280px; width: 100%;\"><img src=\"http://www.supergprix.com/public/img/mailing.png\" alt=\"mail\" style=\"text-align: center; width: 100%;\" /></div>" );

                  if( $this->email->send() ){
                      $this->session->unset_userdata('correo');
                      if (!$this->session->has_userdata('compartir')) {
                          $this->session->unset_userdata('id_user');
                      }
                      $response["codigo"] = 1;
                      $response["mensaje"] = "éxito";
                  }else{
                      $response["codigo"] = 0;
                      $response["mensaje"] = $this->email->print_debugger();
                  }
                  /////////////////////////////////////////////////////////////////////////
                } catch (Exception $e) {
                    $response["codigo"] = 0;
                    $response["mensaje"] =  $e->getTraceAsString();
                }
          } else {
              $response["codigo"] = 0;
              $response["mensaje"] = "no encontró participante con ese id";
          }
      } else {
          $response["codigo"] = 0;
          $response["mensaje"] = "no encontró id en sesión";
      }
      header('Content-type: application/json');
      echo json_encode($response);
  }
}