function centrarVert(elemento){
	altura1 = elemento.parent().height();
	console.log(altura1);
	altura2=elemento.height();
	// altura2 = elemento.children('a').height();
	console.log(altura2);
	elemento.css('margin-top',(altura1/2)-(altura2/2)+'px');
	console.log((altura1/2)-(altura2/2)+'px');
}
function centrarVertP(elemento){
	altura1 = elemento.parent().height();
	console.log(altura1);
	altura2=elemento.height();
	// altura2 = elemento.children('a').height();
	console.log(altura2);
	elemento.css('padding-top',(altura1/2)-(altura2/2)+'px');
	console.log((altura1/2)-(altura2/2)+'px');
}
$(window).on('resize', function () {
	$('#upper-right-col div').height($('#upper-left-col').height());
	$('.contenedor-centrar').each(function () {
		centrarVert($(this));
		console.log('ok');
	});
	$('.contenedor-centrarP').each(function () {
		centrarVertP($(this));
		console.log('ok');
	});
});
$(document).ready(function (){
	$('.contenedor-centrar').each(function () {
		centrarVert($(this));
		console.log('ok');
	});
	$('.contenedor-centrarP').each(function () {
		centrarVertP($(this));
		console.log('ok');
	});
	for (var days_lower_limit = 1; days_lower_limit <= 31; days_lower_limit++) {
		$('select[name="user-birth-day"]').append('<option>' + days_lower_limit + '</option>');
	}
	var months_arr = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
	for (var months_lower_limit = 0; months_lower_limit <= 11; months_lower_limit++) {
		$('select[name="user-birth-month"]').append('<option>' + months_arr[months_lower_limit] + '</option>');
	}
	for (var years_lower_limit = 1900; years_lower_limit <= 2020; years_lower_limit++) {
		$('select[name="user-birth-year"]').append('<option>' + years_lower_limit + '</option>');
	}
	var cities_arr = [];

	$("#user-bill").change(function () {
	    if ($(this).val() != "") {
	        $(".img-adjuntar").attr('src', js_base_url('public/img/boton-adjuntar-check.png'));
	        $("#btnAdjuntarWeb").attr('disabled','disabled');
	        $("#btnAdjuntarMovil").attr('disabled','disabled');
	    }else{
	        $(".img-adjuntar").attr('src', js_base_url('public/img/boton-adjuntar.png'));
	        $("#btnAdjuntarWeb").removeAttr('disabled');
	        $("#btnAdjuntarMovil").removeAttr('disabled');
	    }
	});
});
$('#myModal').on('hide.bs.modal', function (e) {
  $('#user-bill').click();
});
$('#myModalMobile').on('hide.bs.modal', function (e) {
  $('#user-bill').click();
});
