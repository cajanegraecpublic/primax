$(document).ready(function() {
	$('#tablaAprobados').DataTable({
	    dom: 'Bfrtip',
        buttons: [
            'csv', 'excel'
        ]
	});
	$("#eliminarDatos").click(function(event) {
		var conf = confirm("¿Seguro que desea eliminar los registros de esta tabla? Esto es irreversible.");
		if (conf) {
			$.ajax({
				url: js_site_url('admin/eliminarDatos')
			})
			.done(function(data) {
				if (data.codigo == 1) {
					alert("Se ha eliminado correctamente.");
					location.reload(true);
				}
			})
			.fail(function() {
				console.log("error");
			});
		}
	});
});